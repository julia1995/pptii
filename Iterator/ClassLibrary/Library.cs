﻿using Interfaces;

namespace ClassLibrary
{
    public class Library : IBookNumerable
    {
        private Book[] books;
        public Library()
        {
            books = new Book[]
            {
                new Book{Name="Усі жінки - відьми"},
                new Book {Name="Алая королева"},
                new Book {Name="Продатся демону"}
            };
        }
        public int Count
        {
            get { return books.Length; }
        }
        public Book this[int index]
        {
            get { return books[index]; }
        }
        public IBookIterator CreateNumerator()
        {
            return new LibraryNumerator(this);
        }
    }
}
