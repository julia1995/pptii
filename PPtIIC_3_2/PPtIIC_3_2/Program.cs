﻿using Builder;
using System;


namespace PPtIIC_3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // содаем объект сборщика
            collector b = new collector();
            Console.WriteLine("Cбор блокнота: ");
            // создаем билдер для блокнота
            notebookBuilder builder = new RnotebookBuilder();
            // собираем
            notebook r = b.Bake(builder);
            Console.WriteLine(r.ToString());
            Console.WriteLine("Cбор тетради: ");
            // оздаем билдер для тетради 
            builder = new WnotebookBuilder();
            notebook w = b.Bake(builder);
            Console.WriteLine(w.ToString());

            Console.Read();
        }
    }
}
