﻿using System.Text;

namespace Builder
{
    public class notebook
    {
        // картон
        public carton carton { get; set; }
        // бумага
        public paper paper { get; set; }
        // краситель
        public colorant colorant { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (carton != null)
                sb.Append(carton.Sort + "\n");
            if (paper != null)
                sb.Append("Бумага \n");
            if (colorant != null)
                sb.Append("Добавка: " + colorant.Name + " \n");
            return sb.ToString();
        }
    }
}
