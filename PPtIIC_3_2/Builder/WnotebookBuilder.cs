﻿namespace Builder
{
    // строитель для тетради
    public class WnotebookBuilder : notebookBuilder
    {
        public override void SetF()
        {
            this.note.carton = new carton { Sort = "Картон" };
        }

        public override void SetS()
        {
            this.note.paper = new paper();
        }

        public override void SetA()
        {
            this.note.colorant = new colorant { Name = "синяя краска" };
        }
    }
}
