﻿namespace Builder
{
    // абстрактный класс строителя
    public abstract class notebookBuilder
    {
        public notebook note { get; private set; }
        public void Createnote()
        {
            note = new notebook();
        }
        public abstract void SetF();
        public abstract void SetS();
        public abstract void SetA();
    }
}
