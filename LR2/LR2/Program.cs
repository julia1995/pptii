﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;


namespace LR2
{
        class Program
        {
            interface Weapon
            {
                void God();
            }
            class BedProduct : Weapon
            {

            public void God()
                {
                    Console.WriteLine("Годность продукта плохая");
                }
            }
            class GoodProduct : Weapon
            {

            public void God()
                {
                    Console.WriteLine("Годность продукта хорошая");
                }
            }
            interface Movement
            {
                void DataDost();
            }
            class datedost : Movement
            {
                public void DataDost()
                {
                    Console.WriteLine("Сегоднишняя доставка");
                }
            }
            class datedostlast : Movement
            {
                public void DataDost()
                {
                    Console.WriteLine("Вчерашняя доставка");
                }
            }
            class College
            {
                private Weapon _events = null;
                private Movement _eventss = null;
                public College(Weapon ie)
                {
                    this._events = ie;
                }
                public void GetEvents()
                {
                    this._events.God();
                }
                public College(Movement iee)
                {
                    this._eventss = iee;
                }
                public void GetEventss()
                {
                    this._eventss.DataDost();
                }
            }
            class SimpleConfigModule : NinjectModule
            {
                private int x;
                public SimpleConfigModule(int e)
                {
                    this.x = e;
                }
                public override void Load()
                {
                    if (x == 1)
                    {
                        Bind<Weapon>().To<BedProduct>();
                        Bind<Movement>().To<datedost>();
                    }
                    else if (x == 2)
                    {
                        Bind<Weapon>().To<GoodProduct>();
                        Bind<Movement>().To<datedostlast>();
                    }
                    Bind<College>().ToSelf();
                }
            }
            static void Main(string[] args)
            {
                Console.WriteLine("Визначення якостi продукту\n" +
                    "1 - Молоко\n" +
                    "2 - Ковбаса");
                int count = Int32.Parse(Console.ReadLine());
                IKernel ninjectKernel = new StandardKernel(new SimpleConfigModule(count));
                College college = ninjectKernel.Get<College>();
                college.GetEvents();
                Console.ReadLine();
            }
        }

}
