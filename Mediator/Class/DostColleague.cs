﻿using System;

namespace Class
{
    public class DostColleague : Colleague
    {
        public DostColleague(Mediator mediator) : base(mediator)
        { }
        public override void Notify(string message)
        {
            Console.WriteLine("Сообщение водителю: " + message);
        }
    }
}
