﻿namespace Class
{
    public class ManagerMediator : Mediator
    {
        public Colleague Customer { get; set; }
        public Colleague Manager { get; set; }
        public Colleague Dost { get; set; }
        public override void Send(string msg, Colleague colleague)
        {
            if (Customer == colleague) Manager.Notify(msg);           
            else if (Manager == colleague) Dost.Notify(msg);         
            else if (Dost == colleague) Customer.Notify(msg);
        }
    }
}
