﻿using Class;
using System;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            ManagerMediator mediator = new ManagerMediator();
            Colleague customer = new CustomerColleague(mediator);
            Colleague manager = new ProgrammerColleague(mediator);
            Colleague dost = new DostColleague(mediator);
            mediator.Customer = customer;
            mediator.Manager = manager;
            mediator.Dost = dost;
            customer.Send("Есть посылка, надо оформить");
            manager.Send("Посылка оформлена, надо доставить");
            dost.Send("Посылка доставлена");

            Console.Read();
        }
    }
}
