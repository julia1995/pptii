﻿using Class;
using System;

namespace PPtIIC_4_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Stock stock = new Stock();
            Bank bank = new Bank("Stock", stock);
            Broker broker = new Broker("Волошко Лариса", stock);
            // имитация торгов
            stock.Market();
            // брокер прекращает наблюдать за торгами
            broker.StopTrade();
            // имитация торгов
            stock.Market();

            Console.Read();
        }
    }
}
