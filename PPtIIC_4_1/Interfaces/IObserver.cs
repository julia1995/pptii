﻿using System;

namespace Interfaces
{
    public interface IObserver
    {
        void Update(Object ob);
    }
}
