﻿using Interfaces;
using System;

namespace Class
{
    public class Bank : IObserver
    {
        public string Name { get; set; }
        IObservable stock;
        public Bank(string name, IObservable obs)
        {
            this.Name = name;
            stock = obs;
            stock.RegisterObserver(this);
        }
        public void Update(object ob)
        {
            StockInfo sInfo = (StockInfo)ob;

            if (sInfo.Vaz > 400)
                Console.WriteLine("Аукцион {0} продает вазу;  стоимость вазы: {1}", this.Name, sInfo.Vaz);
            else
                Console.WriteLine("Аукцион {0} покупает вазу;  стоимость вазы: {1}", this.Name, sInfo.Vaz);
        }
    }
}
