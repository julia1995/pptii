﻿using Interfaces;
using System;

namespace Class
{
    public class Broker : IObserver
    {
        public string Name { get; set; }
        IObservable stock;
        public Broker(string name, IObservable obs)
        {
            this.Name = name;
            stock = obs;
            stock.RegisterObserver(this);
        }
        public void Update(object ob)
        {
            StockInfo sInfo = (StockInfo)ob;

            if (sInfo.Kart > 300)
                Console.WriteLine("Клиент {0} продает картину;  стоимость картины: {1}", this.Name, sInfo.Kart);
            else
                Console.WriteLine("Клиент {0} покупает картину;  стоимость картины: {1}", this.Name, sInfo.Kart);
        }
        public void StopTrade()
        {
            stock.RemoveObserver(this);
            stock = null;
        }
    }
}
