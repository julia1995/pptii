﻿using Interfaces;
using System;

namespace ClassLibrary
{
    public class Auto : ITransport
    {
        public void Drive()
        {
            Console.WriteLine("Машина едет по дороге");
        }
    }
}
