﻿using Interfaces;

namespace ClassLibrary
{
    public class Driver
    {
        public void Travel(ITransport transport)
        {
            transport.Drive();
        }
    }
}
