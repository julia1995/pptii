﻿namespace Interfaces
{
    public interface IAnimal
    {
        void Move();
    }
}
