﻿namespace Interfaces
{
    public interface ITransport
    {
        void Drive();
    }
}
