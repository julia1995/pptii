﻿namespace Class
{
    public class Client
    {
        public void HB(HBFacade facade)
        {
            facade.Start();
            facade.Stop();
        }
    }
}
