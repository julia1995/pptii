﻿namespace Class
{
    public class HBFacade
    {
        Shop shop;
        Packaging packaging;
        Presentation presentation;
        public HBFacade(Shop s, Packaging p, Presentation pp)
        {
            this.shop = s;
            this.packaging = p;
            this.presentation = pp;
        }
        public void Start()
        {
            shop.Choice_gift();
            shop.Buy();
            packaging.Wrapper();
            presentation.Execute();
        }
        public void Stop()
        {
            presentation.Finish();
        }
    }
}
