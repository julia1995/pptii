﻿using Class;
using System;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop s = new Shop();
            Packaging p = new Packaging();
            Presentation pp = new Presentation();

            HBFacade i = new HBFacade(s, p, pp);

            Client client = new Client();
            client.HB(i);

            Console.Read();
        }
    }
}
