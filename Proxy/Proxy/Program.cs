﻿using ClassLibrary;
using System;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            IBook subject = new BookStoreProxy();
            subject.Request();
            Console.Read();
        }
    }

}
