﻿using System;

namespace ClassLibrary
{
    public class BookStore : IBook
    {
        public override void Request()
        {
            Console.WriteLine("BookStore");
        }
    }
}
