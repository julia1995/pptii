﻿namespace ClassLibrary
{
    public class BookStoreProxy : IBook
    {
        BookStore realSubject;
        public override void Request()
        {
            if(realSubject == null)
            realSubject = new BookStore();
            realSubject.Request();
        }
    }
}
