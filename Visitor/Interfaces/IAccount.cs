﻿namespace Interfaces
{
    public interface IAccount
    {
        void Accept(IVisitor visitor);
    }
}
