﻿using System;

namespace Interfaces
{
    public interface IVisitor
    {
        void VisitPersonAcc(Version acc);
        void VisitCompanyAc(Company acc);
    }
}
