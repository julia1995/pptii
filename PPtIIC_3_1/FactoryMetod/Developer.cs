﻿namespace FactoryMetod
{
    // абстрактный класс 
   public abstract class Developer
    {
        public string Name { get; set; }

        public Developer(string n)
        {
            Name = n;
        }
        // фабричный метод
        abstract public chancery Create();
    }
}
