﻿namespace FactoryMetod
{
    // собирает ручку
    public class Developer2 : Developer
    {
        public Developer2(string n) : base(n)
        { }

        public override chancery Create()
        {
            return new pen();
        }
    }
}
