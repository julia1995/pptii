﻿namespace FactoryMetod
{
    // собирает тетрадь
    public class Developer1 : Developer
    {
        public Developer1(string n) : base(n)
        { }

        public override chancery Create()
        {
            return new notebook();
        }
    }
}
