﻿using FactoryMetod;
using System;

namespace PPtIIC_3_1
{
     class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1 вересня");
            Developer dev = new Developer1("1 вересня");
            chancery chancery1 = dev.Create();
            Console.WriteLine("Школярик");
            dev = new Developer2("Школярик");
            chancery chancery2 = dev.Create();

            Console.ReadLine();
        }
    }
}
