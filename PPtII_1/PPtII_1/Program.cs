﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPtII_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Pr mol = new Pr(new MolFactory());
            mol.God();
            mol.DataDost();
            Pr kov = new Pr(new KovFactory());
            kov.God();
            kov.DataDost();
            Console.ReadLine();
        }
        //Абстрактный класс годности продукта
        abstract class Weapon
        {
            public abstract void God();
        }
        //Абстрактный класс даты доставки
        abstract class Movement
        {
            public abstract void DataDost();
        }
        //Класс хорошей годности продукта
        class GoodProduct : Weapon
        {
            public override void God()
            {
                Console.WriteLine("Годность продукта хорошая");
            }
        }
        //Класс плохой годности продукта
        class BedProduct : Weapon
        {
            public override void God()
            {
                Console.WriteLine("Годность продукта плохая");
            }
        }
        //Класс сегоднешней доставки
        class datedost : Movement
        {
            public override void DataDost()
            {
                Console.WriteLine("Сегоднишняя доставка");
            }
        }
        //Класс вчерашней доставки
        class datedostlast : Movement
        {
            public override void DataDost()
            {
                Console.WriteLine("Вчерашняя доставка");
            }
        }
        //Клас абстрактной фабрики
        abstract class HeroFactory
        {
            public abstract Movement CreateMovement();
            public abstract Weapon CreateWeapon();
        }
        // Фабрика создания молочных изделий
        class MolFactory : HeroFactory
        {
            public override Movement CreateMovement()
            {
                return new datedost();
            }

            public override Weapon CreateWeapon()
            {
                return new GoodProduct();
            }
        }
        // Фабрика создания ковбасных изделий
        class KovFactory : HeroFactory
        {
            public override Movement CreateMovement()
            {
                return new datedostlast();
            }

            public override Weapon CreateWeapon()
            {
                return new BedProduct();
            }
        }
        // Клиент - продукт
        class Pr
        {
            private Weapon weapon;
            private Movement movement;
            public Pr(HeroFactory factory)
            {
                weapon = factory.CreateWeapon();
                movement = factory.CreateMovement();
            }
            public void DataDost()
            {
                movement.DataDost();
            }
            public void God()
            {
                weapon.God();
            }
        }
    }
}
