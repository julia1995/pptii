﻿using ClassLibrary;
using Interfaces;
using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Скопировать прямоугольник - 1");
            Console.WriteLine("Скопировать круг - 2");
            int k= Convert.ToInt32(Console.ReadLine());
            if (k == 1)
            {
                IFigure figure = new Rectangle(30, 40);
                IFigure clonedFigure = figure.Clone();
                figure.GetInfo();
                clonedFigure.GetInfo();
            }
            else if (k==2)
            {
                IFigure figure = new Circle(30, 50, 60);
                IFigure clonedFigure = figure.Clone();
                figure.GetInfo();
                clonedFigure.GetInfo();
            }
            Console.Read();
        }
    }
}

