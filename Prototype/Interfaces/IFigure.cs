﻿namespace Interfaces
{
    public interface IFigure
    {
        IFigure Clone();
        void GetInfo();
    }
}
