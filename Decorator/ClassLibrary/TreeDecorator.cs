﻿namespace ClassLibrary
{
    public abstract class TreeDecorator : Tree
    {
        protected Tree tree;
        public TreeDecorator(string n, Tree tree) : base(n)
        {
            this.tree = tree;
        }
    }
}
