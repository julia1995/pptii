﻿namespace ClassLibrary
{
    public class Bow : TreeDecorator
    {
        public Bow(Tree p)
        : base(p.Name + ", украшена бантами", p)
        { }

        public override double GetCost()
        {
            return tree.GetCost() * 1.25;
        }
    }
}
