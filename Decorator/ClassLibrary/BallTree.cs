﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
   public class BallTree : TreeDecorator
    {
        public BallTree(Tree p)
        : base(p.Name + ", украшена шарами", p)
        { }

        public override double GetCost()
        {
            return tree.GetCost() * 1.67;
        }
    }
}
