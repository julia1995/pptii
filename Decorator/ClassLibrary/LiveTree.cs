﻿namespace ClassLibrary
{
    public class LiveTree : Tree
    {
        public LiveTree() : base("Живая ёлка")
        { }
        public override double GetCost()
        {
            return 120;
        }
    }
}
