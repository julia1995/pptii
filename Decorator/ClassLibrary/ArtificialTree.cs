﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class ArtificialTree : Tree
    {
        public ArtificialTree() : base("Искусственная ёлка")
        { }
        public override double GetCost()
        {
            return 250;
        }
    }
}
