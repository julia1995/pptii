﻿namespace ClassLibrary
{
    public abstract class Tree
    {
        public Tree(string n)
        {
            this.Name = n;
        }
        public string Name { get; protected set; }
        public abstract double GetCost();
    }
}
