﻿using ClassLibrary;
using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree tree1 = new LiveTree();
            tree1 = new BallTree(tree1); // живая ёлка украшеная шарами
            Console.WriteLine("Название: {0}", tree1.Name);
            Console.WriteLine("Цена: {0}", tree1.GetCost());

            Tree tree2 = new ArtificialTree();
            tree2 = new Bow(tree2);// искусственная ёлка украшеная бантами
            Console.WriteLine("Название: {0}", tree2.Name);
            Console.WriteLine("Цена: {0}", tree2.GetCost());

            Tree tree3 = new LiveTree();
            tree3 = new BallTree(tree3);
            tree3 = new Bow(tree3);// живая ёлка украшеная шарами и бантами
            Console.WriteLine("Название: {0}", tree3.Name);
            Console.WriteLine("Цена: {0}", tree3.GetCost());

            Console.ReadLine();
        }
    }
}
