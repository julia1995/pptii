﻿using Interface;

namespace Class
{
    public class FCommand : ICommand_1
    {
        First first;
        public FCommand(First first)
        {
            this.first = first;
        }
        public void ToTake()
        {
            first.ToTake();
        }
        public void NoTake()
        {
            first.NoTake();
        }
    }
}
