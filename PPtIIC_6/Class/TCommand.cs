﻿using Interface;

namespace Class
{
    public class TCommand : ICommand_3
    {
        Third Third;
        public TCommand(Third Third)
        {
            this.Third = Third;
        }
        public void ToGive()
        {
            Third.ToGive();
        }
        public void NoGive()
        {
            Third.NoGive();
        }
    }
}
