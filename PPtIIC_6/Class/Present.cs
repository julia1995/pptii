﻿namespace Class
{
    public class Present
    {
        public bool BuyPresent { get; set; }
        public bool PackegPresent { get; set; }
        public bool PresentationPresent { get; set; }
        public Present(bool bp, bool pp, bool ppt)
        {
            BuyPresent = bp;
            PackegPresent = pp;
            PresentationPresent = ppt;
        }
    }
}
