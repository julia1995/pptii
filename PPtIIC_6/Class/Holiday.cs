﻿using Interface;

namespace Class
{
    public class Holiday
    {
        ICommand_3 command;

        public Holiday() { }

        public void SetCommand(ICommand_3 com)
        {
            command = com;
        }

        public void ToGive()
        {
            command.ToGive();
        }
        public void NoGive()
        {
            command.NoGive();
        }
    }
}
