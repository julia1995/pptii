﻿using System;

namespace Class
{
    public class Third
    {
        public void ToGive()
        {
            Console.WriteLine("Подарок принесен");
        }

        public void NoGive()
        {
            Console.WriteLine("Подарок отдан");
        }
    }
}
