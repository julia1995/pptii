﻿using Interface;

namespace Class
{
    public class SCommand : ICommand_2
    {
        Second second;
        public SCommand(Second second)
        {
            this.second = second;
        }
        public void ToPackeg()
        {
            second.ToPackeg();
        }
        public void NoPackeg()
        {
            second.NoPackeg();
        }
    }
}
