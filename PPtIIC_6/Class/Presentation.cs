﻿namespace Class
{
    public abstract class Presentation
    {
        public Presentation presentation { get; set; }
        public abstract void present(Present prt);
    }
}
