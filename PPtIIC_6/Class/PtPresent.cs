﻿namespace Class
{
    public class PtPresent : Presentation
    {
        public override void present(Present prt)
        {
            if (prt.PresentationPresent == true)
            {
                Holiday hol = new Holiday();
                Third t = new Third();
                hol.SetCommand(new TCommand(t));
                hol.ToGive();
                hol.NoGive();
            }
            else if (presentation != null)
                presentation.present(prt);
        }
    }
}
