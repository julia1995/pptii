﻿using Interface;

namespace Class
{
    public class Market
    {
        ICommand_1 command;

        public Market() { }

        public void SetCommand(ICommand_1 com)
        {
            command = com;
        }

        public void ToTake()
        {
            command.ToTake();
        }
        public void NoToTake()
        {
            command.NoTake();
        }
    }
}
