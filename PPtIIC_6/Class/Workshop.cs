﻿using Interface;

namespace Class
{
    public class Workshop
    {
        ICommand_2 command;

        public Workshop() { }

        public void SetCommand(ICommand_2 com)
        {
            command = com;
        }

        public void ToPackeg()
        {
            command.ToPackeg();
        }
        public void NoPackeg()
        {
            command.NoPackeg();
        }
    }
}
